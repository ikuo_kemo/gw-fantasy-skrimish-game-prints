%\section{FIGHTING THE BATTLE}

A Warcry battle is fought in a series of rounds referred to as
\textbf{battle} rounds, each of which is split into three phases: the
\textbf{initiative phase}, the \textbf{reserve phase} and the
\textbf{combat phase}.  Once all phases have finished, a new battle
round begins. The victory condition will dictate after which battle
round the battle ends and the winner is decided.

\subsection{BATTLE ROUND SEQUENCE}

\begin{enumerate}
  \item \textbf{INITIATIVE PHASE}\\ Players determine who has the
    initiative during the battle round, and then they decide how to use
    their wild dice.

  \item \textbf{RESERVE PHASE}\\ In battle rounds after the first,
    reserve fighters may arrive on the battlefield.

  \item \textbf{COMBAT PHASE}\\ Players take it in turns to activate a
    fighter in their warband.
\end{enumerate}

\subsection{INITIATIVE PHASE}
\label{ssec:initaitve-phase}

At the start of the initiative phase, each player rolls 6 dice. These
dice are the \textbf{initiative dice}. After rolling, each player
counts how many \textbf{singles} they have. A single is a dice with a
score that does not match the score on any other dice in that player’s
roll.

The player with the most singles has the \textbf{initiative}. In the
case of a tie, the players roll off and the winner has the initiative.

The remaining dice are referred to as \textbf{ability dice}, and can
be used to perform \textbf{abilities} in the combat phase (pg \pageref{ssec:combat-phase}). If 2
of your ability dice have the same score, it is referred to as a
\textbf{[double]}. If 3 of your ability dice have the same score, it
is referred to as a \textbf{[triple]}.  Finally, if 4 or more of your
ability dice have the same score, it is referred to as a
\textbf{[quad]}.

At the start of each battle round, any remaining singles and ability
dice from the previous battle round are discarded.

\subsubsection{WILD DICE}

After the initiative has been determined, each player receives 1 extra
dice known as a \textbf{wild dice}. A wild dice can be used during the
initiative phase to add to either a player’s singles or ability dice.

The player with the initiative first declares how they will use each
of their wild dice, followed by the player who does not have the
initiative.

Wild dice can be used in the following ways:

\begin{itemize}
  \item A wild dice can be used to add 1 to the number of singles the
player has. In this case, it does not matter what the score of the
wild dice is. Any number of wild dice can be used in this way and are
discarded at the end of the battle round.
  \item A wild dice can be used to turn 1 of your singles into an
ability dice \textbf{[double]}, to improve a \textbf{[double]} to a
\textbf{[triple]}, or to improve a \textbf{[triple]} to a
\textbf{[quad]}.  In this case, the score of the wild dice is set to
match the score of the single or ability dice it is paired with. You
cannot add multiple wild dice to the same single or same ability dice
(e.g.  you cannot turn a [double] into a [quad]). Wild dice that are
used in this way and that remain at the end of the battle round are
discarded in the same manner as your other ability dice.
  \item A wild dice can be saved to be used in a later battle
round. If you choose to do so, place it to one side. In the next
battle round, you can use that wild dice in addition to the one you
gain in that battle round. Wild dice can be saved multiple times, and
you can save multiple wild dice at once (for example, you could save
up all your wild dice until the final battle round and then use them
all at once!).
\end{itemize}

\textbf{Seizing the Initiative}\\ Once both players have declared how
they will use their wild dice in the battle round, count the number of
singles each player has once more. If the player without the
initiative now has more singles than the player with the initiative,
they now have the initiative instead. If the number of singles each
player has is now tied (and was not previously), the players roll off
and the winner has the initiative.

\subsubsection{EXAMPLE INITIATIVE PHASE}
\import{includes/}{example-initaitve-phase}

\subsection{RESERVE PHASE}

The reserve phase comes into play in battle rounds after the
first. During the reserve phase, fighters in reserve battle groups may
be set up on the battlefield.  The deployment map will indicate which
reserve battle groups come into play and in which battle round: ‘RND2’
indicates that the specified battle group will arrive in the reserve
phase of the second battle round, and ‘RND3’ indicates it will arrive
in the reserve phase of the third battle round.

Starting with the player with the initiative, players set up the
fighters from the battle groups that are coming into play in that
battle round wholly within 3" horizontally of their deployment point.

Some deployment maps have reserve deployment points that are situated
off the battlefield. In these cases, there will be a
\textbf{deployment line} next to that deployment point.  Deployment
lines mark either the length of half a battlefield edge or the length
of a whole battlefield edge. If a deployment point has a deployment
line, fighters from that battle group must be set up wholly within 3"
horizontally of the marked portion of the battlefield edge.

If it is ever impossible to set up all the fighters from a reserve
battle group (for example, due to the positions of enemy fighters),
each fighter from that battle group must be set up one at a time, as
close as possible to their deployment point (if it is on the
battlefield map) or deployment line.

\subsection{COMBAT PHASE}
\label{ssec:combat-phase}

In the combat phase, the players take it in turns to \textbf{activate}
a fighter.  The player with the initiative picks which player takes
the first turn.

When it is a player’s turn, they can activate 1 fighter in their
warband. This is referred to as that fighter’s
\textbf{activation}. The player must pick a fighter to activate if
they can, but cannot pick a fighter that has already been activated in
that phase. If the player cannot pick a fighter (for example, if all
their fighters have already been activated in that phase), they must
\textbf{pass}. Then their opponent can activate a fighter or
pass. Keep on taking turns to activate fighters until both players
pass consecutively.

\subsubsection{ACTIONS}

When a player activates a fighter, that fighter makes 2
\textbf{actions} chosen by the player from the list below. The player
carries out the first action before deciding on the second. The
fighter can make the same action twice in a row if the player wishes
(for example, a move action followed by a move action).
\begin{itemize}
\item MOVE (pg \pageref{sec:move-actions})
\item ATTACK (pg \pageref{sec:attack-actions})
\item DISENGAGE (pg \pageref{sec:disengage-actions})
\item WAIT (pg \pageref{sec:wait-actions})
\end{itemize}

\subsubsection{ABILITIES}

In addition to their 2 actions, a fighter can use 1
\textbf{ability}. An ability can be used before the fighter’s 2
actions, or after either their first or second action. Abilities are
explained on page 14.

\subsubsection{REACTIONS}

Lastly, fighters can make \textbf{reactions} during an enemy fighter’s
activation, such as countering an attack or ducking for cover.
Reactions are explained on page 15.

\subsubsection{ENDING THE BATTLE}

The victory condition will dictate after which battle round the battle
ends and which player is declared the winner. When the battle ends, if
neither player has achieved the victory condition, the battle is a
\textbf{draw} and neither player wins.

\subsubsection{Bonus Actions}

Certain rules may allow for a fighter to make a \textbf{bonus action}.
For example the ‘Rampage’ ability (pg
\pageref{table:universal-abilities}) allows a fighter to make a bonus
move action and then a bonus attack action.

A fighter can make any number of bonus actions in addition to their 2
actions.
