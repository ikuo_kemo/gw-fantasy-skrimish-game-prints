%\section{ATTACK ACTIONS}

Every fighter has 1 or more \textbf{weapons} detailed on their
profile, which they use to make \textbf{attack actions}. Each weapon
has 4 characteristics: \textbf{Range}, \textbf{Attacks},
\textbf{Strength} and \textbf{Damage}.

When a fighter makes an attack action, the characteristics of the
attack action are determined by the weapon being used for that attack
action (for example, the Strength characteristic of an attack action
matches the Strength characteristic of the weapon being used).

To make an attack action with a fighter, follow these steps:

\subsection{1. PICK A WEAPON AND TARGET}

Pick 1 of the fighter’s weapons to be used and pick 1 visible enemy
fighter within range to be the \textbf{target} of the attack action.

If there are any enemy fighters within 1" of the attacking fighter,
one of those enemy fighters must be picked to be the target. If there
are no enemy fighters within 1" of the attacking fighter, you can pick
any enemy fighter to be the target as long as they are within range of
the weapon being used.

Lastly, missile attack actions (see right) cannot be made against
enemy fighters that are within 1" of another fighter from the
attacking fighter’s warband – the risk of hitting your ally is too
great!

\subsection{2. ROLL TO HIT}

Roll a number of dice equal to the \textbf{Attacks} characteristic of
the weapon being used. Each of these dice rolls is referred to as a
\textbf{hit roll.}

Next, you will need to determine which of the dice have
\textbf{missed}, which have scored a \textbf{hit} and which have
scored a \textbf{critical hit}. To do so, compare the Strength
characteristic of the weapon being used with the Toughness
characteristic of the target fighter and consult the table below:

\import{includes/}{hit-table}

\subsection{3. TOTAL DAMAGE}

The \textbf{Damage} characteristic of a weapon has 2 values (divided
by a ‘/’). These determine how many damage points are allocated to the
enemy fighter targeted by that attack action. For each \textbf{hit},
allocate a number of damage points equal to the first value of the
Damage characteristic. For each \textbf{critical hit}, allocate a
number of damage points equal to the second value of the Damage
characteristic.

For example, if a weapon with a Damage characteristic of 1/3 is being
used and the attack action scores 2 hits, 1 critical hit and 1 miss,
the total number of damage points allocated to the target fighter of
that attack action would be 5 (1+1+3+0).

\subsection{Range}

The range of an attack action is a number of inches equal to the
\textbf{Range} characteristic of the weapon being used. For example,
if the weapon being used has a Range characteristic of 3, an enemy
fighter within 3" of the attacking fighter could be targeted by that
attack action.

The Range characteristic of some weapons includes a minimum range and
a maximum range. This means the attack action cannot target fighters
that are within a certain range of the attacking fighter. For example,
if the weapon being used has a Range characteristic of 6-20, the
attack action can target a fighter within 20" but not if that fighter
is within 6".

\subsection{Melee Attack Actions vs Missile Attack Actions} There are
2 types of attack actions: \textbf{melee attack actions} and
\textbf{missile attack actions}.  The attack action’s type is
determined by the Range characteristic of the weapon being used. A
melee attack action is an attack action made with a weapon that has a
Range characteristic of 3 or less. A missile attack action is an
attack action made with a weapon that has a Range characteristic
greater than 3.

If the weapon has both a minimum and maximum range, the maximum range
is considered to be the Range characteristic when determining if it is
a melee attack action or a missile attack action.

\subsection{Allocating Damage}
\label{ssec:allocating-damage}

Once the damage of an attack action has been determined, the damage is
allocated to the target fighter. Fighters can also have damage points
allocated to them in other ways such as falling from height. Damage is
allocated as follows:

\begin{itemize}
  \item Damage points are allocated one at a time.
  \item If the number of damage points allocated to a fighter equals
    its \textbf{Wounds} characteristic, that fighter is \textbf{taken
      down}. Remove that fighter from the battlefield.
  \item A taken down fighter takes no further part in the battle –
    they cannot be activated, cannot make actions and cannot use
    abilities.
  \item When a fighter is taken down, any leftover damage points from
    the attack action are discarded.
\end{itemize}
