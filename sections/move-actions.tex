%\section{MOVE ACTIONS}

Fighters can move across the battlefield by making a \textbf{move
action}. Every fighter has a \textbf{Move characteristic}, shown on
their \textbf{profile}, which determines the number of inches a
fighter can move in total in a single move action.

When a fighter makes a move action, there are 4 ways in which they can
move: \textbf{move normally}, \textbf{jump}, \textbf{climb} and
\textbf{fly}. A fighter can move in any combination of these ways as
part of a single move action, so long as the total distance in inches
moved does not exceed the fighter’s Move characteristic.

As a fighter moves across the battlefield, they can pivot freely so
long as at the end of the move action no part of the fighter is
further from its starting position than a number of inches equal to
its Move characteristic.

There are general limitations a fighter must adhere to when making a
move action:

\begin{itemize}
  \item A fighter cannot move through other fighters.
  \item A fighter cannot move through any part of a terrain
  \item No part of a fighter can move over the battlefield edge.
  \item A fighter cannot start a move action if they are within 1" of
    any enemy fighters. They must make a \textbf{disengage action} instead
    if they wish to move away (pg \pageref{sec:disengage-actions}).
\end{itemize}

\subsection{MOVING NORMALLY}

During a move action, a fighter can move normally whenever the centre
of their base is on the battlefield floor or on a platform (pg
16). When a fighter moves normally, the centre of their base must
remain on the battlefield floor or a platform at all times, unless
they are moving over \textbf{low terrain} (pg \pageref{sec:terrain}).

\subsection{JUMPING}

During a move action, a fighter can \textbf{jump}. If they do so, the
fighter moves in a straight line horizontally through the air, and can
move any distance vertically downwards through the air.

When a fighter jumps, count the horizontal distance moved towards the
number of inches that fighter can move in total in that move action as
normal, but do not count the distance moved vertically
downwards. However, if the fighter moves 2" or more vertically
downwards when jumping, they suffer \textbf{impact damage} (pg
\pageref{ssec:impact-damage}) at the end of that move action.

If a fighter finishes their move action still in the air, immediately
move them vertically downwards until a part of their base is either on
or touching part of a terrain feature or the battlefield floor.  If
the fighter moves 2" or more vertically downwards in this manner, they
suffer \textbf{impact damage} (pg \pageref{ssec:impact-damage}).

\subsection{CLIMBING}

During a move action, if a fighter is touching a part of a terrain
feature that is an \textbf{obstacle} (pg 16), they can begin to
\textbf{climb}. While climbing, a fighter can move vertically up or
down through the air as well as horizontally, but they must stay
within \nicefrac{1}{2}" of the obstacle they are climbing at all
times. Once a fighter begins to climb, they are said to be climbing
until the centre of their base is on the battlefield floor or a
platform, or until they jump or fly.  A fighter can finish a move
action while climbing, but if they are still climbing when their
activation ends, they \textbf{fall} (see right).

Fighters with the \textbf{Mount} runemark (\inlinerunemark{rm-mount})
cannot climb, with the exception of \textbf{climbing stairs} (pg 
\pageref{sssec:staris-and-ladders}).

\subsection{FLYING}

Fighters with the \textbf{Fly} runemark (\inlinerunemark{rm-fly}) can
\textbf{fly} during a move action.  If they do so, the fighter can
move through the air vertically and horizontally. Count the horizontal
distance moved towards the number of inches that fighter can move in
total in that move action as normal, but do not count the distance
moved vertically.  Flying effectively allows a fighter to pass over
terrain features and other fighters.

Once a fighter begins to fly, they are said to be flying until the
centre of their base is on the battlefield floor or a platform. A
fighter cannot end a move action flying.

\subsection{Falling}
\label{ssec:falling}

There are a few situations that can cause a fighter to \textbf{fall}.
Firstly, if a fighter finishes a move action with the centre of their
base not on the battlefield floor or a platform, and they are not
climbing, that fighter falls.

Secondly, if a fighter is still climbing \textbf{when their activation
ends}, they fall.

Lastly, fighters may fall as a result of being attacked near the edge
of a platform (pg \pageref{sssec:terrain-platforms}).

If a fighter falls, the opposing player picks a point on a platform or
the battlefield floor that is within 2" horizontally of the fighter
that has fallen and that is vertically lower than the fighter that has
fallen. The fallen fighter is then placed with the centre of their
base on that point.

The player cannot pick a point that would cause the fallen fighter to
be placed on or through another fighter or through a terrain
feature. If it is impossible not to do so, and the centre of the base
of the fallen fighter is on a platform, they remain where they are. If
the centre of the base of the fallen fighter is not on a platform,
they are immediately \textbf{taken down} (pg
\pageref{ssec:allocating-damage}) instead.

If the fallen fighter is now 2" or more vertically lower than their
location before they fell, they suffer impact damage (see right).

\subsection{Impact Damage} 
\label{ssec:impact-damage}

If a fighter suffers \textbf{impact damage}, roll a dice. On a 1,
allocate 3 damage points to that fighter (pg
\pageref{ssec:allocating-damage}). On a 2-3, allocate 1 damage point
to that fighter. On a 4+, nothing happens.

\subsection{Disallowed Moves}

If a fighter’s move causes them to break any of the limitations of
move actions, it is referred to as a \textbf{disallowed move}. For
example, if a fighter jumped and the vertical distance moved downwards
caused them to pass through another fighter, this would be a
disallowed move.  Fighters cannot make

disallowed moves. If one occurs during a move action, place the
fighter making the move action back at their starting position and
choose a new direction for them to move.

Remember –- as players are allowed to pre-measure any distances, you
can plan your move action ahead to make sure it is not a disallowed
move.
