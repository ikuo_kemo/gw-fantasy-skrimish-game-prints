%\section{TERRAIN}

In every Warcry battle, there will be 1 or more \textbf{terrain
features}.  Fighters can interact with terrain features in numerous
ways when making move actions (pg \pageref{sec:move-actions}).  This
section introduces the other interactions fighters can have with
terrain features, and also describes the different types of terrain
features.

It is important to note that a terrain feature can be made up of
multiple types of terrain as detailed in this section. For example, in
the case of a ruined building, its walls would be \textbf{obstacles},
the upper floors would be \textbf{platforms}, it might have a door
built into it, and any debris around it would be \textbf{low terrain}.

\subsubsection{OBSTACLES}

The most common type of terrain is an \textbf{obstacle}. An obstacle
is any part of a terrain feature that prevents a fighter from moving
horizontally and that extends vertically 1" or more from the
battlefield floor or the platform upon which it is placed. A wall is
an example of an obstacle.

\subsubsection{LOW TERRAIN}

Any part of a terrain feature that extends vertically less than 1"
from the battlefield floor or the platform upon which it is placed is
known as \textbf{low terrain}. When a fighter moves normally, they can
move without penalty over low terrain. This means that any vertical
distance moved while moving over such a part of a terrain feature does
not count against the total number of inches fighters can move in that
move action. In addition, low terrain is treated as part of the
battlefield floor or the platform upon which it is placed.

\subsubsection{PLATFORMS}
\label{sssec:terrain-platforms}

Another common type of terrain feature is a \textbf{platform}. A
platform is a horizontally flat surface on a terrain feature with a
surface area larger than 1" by 1".

\textbf{Falling Off Terrain}\\

When a fighter within \nicefrac{1}{2}" of the edge of an open platform
(i.e.  an edge that is not enclosed by an obstacle, such as a wall) is
targeted by an attack action, their controlling player must take a
\textbf{falling test} for them after the attack action has been
resolved

To take a falling test for a fighter, roll a dice. On a 1, the fighter
\textbf{falls} (pg \pageref{ssec:falling}). This rule does not affect
fighters with the \textbf{Fly} runemark (\inlinerunemark{rm-fly}).

\subsubsection{DEADLY TERRAIN}

Spiked walls and fences, including those made from jutting rib bones,
are \textbf{deadly terrain}. Deadly terrain is treated as an obstacle,
but when a fighter begins to climb it, allocate D6 damage points to
that fighter first. In addition, when a fighter is placed after
falling (pg \pageref{ssec:falling}), if they are placed within 1" of
any deadly terrain, allocate D6 damage points to that fighter.

\textbf{UNSCALABLE TERRAIN}

Another type of terrain is unscalable terrain. The following parts of
terrain features are unscalable terrain:

\begin{itemize}
  \item Statues
  \item Braziers
  \item Tree branches and foliage
  \item Fountains
\end{itemize}

Unscalable terrain is treated as an obstacle with the following
exception: fighters cannot climb or move on unscalable terrain.  If a
fighter ends an action on unscalable terrain, they first suffer
\textbf{impact damage} and then they \textbf{fall}.

\subsubsection{STAIRS AND LADDERS}
\label{sssec:staris-and-ladders}

\textbf{Stairs} and \textbf{ladders} are treated as obstacles, with
the following exceptions:

\begin{itemize}
  \item Fighters that finish their activation climbing stairs or
    ladders do not fall and can remain part way up (if it is not possible
    to physically place the fighter in their current location, make a note
    of where they are).
  \item When an attack action targets an enemy fighter that has ended
    their activation climbing stairs or ladders, the target fighter must
    take a falling test after the attack action has been resolved.
  \item Fighters with the Mount runemark (\inlinerunemark{rm-mount}) can
    climb stairs.
\end{itemize}

\subsubsection{ARCHWAYS AND DOORS}

A terrain feature may have \textbf{archways} or \textbf{doors}.

As part of a move action, if a fighter comes into contact with an
archway or a door, that fighter can move normally through it even if
the model or its base is too large to physically fit through (or it is
blocked completely, as in the case of a closed door). This is an
exception to the rule that states that a fighter cannot move through
any part of a terrain feature.

To move through an archway or a door, first measure the distance in a
straight line through the horizontal centre of the archway or door. If
the fighter has sufficient movement to pass through the archway or
door and be placed on the other side, they can move through it.

When fighters move through archways and doors, all other movement
rules must still be followed (for example, they cannot move through
another fighter).

\textbf{\large Restrictions}\\ Fighters with any of the following
runemarks cannot move through archways:

\begin{itemize}
  \item \textbf{Monster} (\inlinerunemark{rm-monster})
  \item \textbf{Mount} (\inlinerunemark{rm-mount})
\end{itemize}

Fighters with any of the following runemarks cannot move through
closed doors:

\begin{itemize}
  \item \textbf{Monster} (\inlinerunemark{rm-monster})
  \item \textbf{Mount} (\inlinerunemark{rm-mount})
  \item \textbf{Beast} (\inlinerunemark{rm-beast})
\end{itemize}

\subsection{COVER}
\label{ssec:cover}

Obstacles and platforms can provide protection to fighters from
attacks. When a fighter is targeted by an attack action, players must
first determine if they are in \textbf{cover}.  If a fighter is in
cover, add 1 to their Toughness characteristic for the duration of
that attack action.

\subsubsection{OBSTACLES}

To determine if a target fighter is in cover due to any obstacles,
draw an imaginary line between the closest points on each fighter’s
base (for rules purposes, the line is considered to be 1mm wide). If
the line passes through an \textbf{obstacle}, the target fighter is in
cover.

If the fighters are more than 1" away from each other, do not count
parts of obstacles within ½" of the fighter making the attack action
(this represents fighters that have weapons with a longer range being
able to aim around corners, through gaps in nearby terrain, and so
on).

\subsubsection{PLATFORMS}

When a fighter on a platform is targeted by an attack action, they are
in cover if the fighter making the attack action is 2" or more
vertically below the target fighter.

\subsubsection{Bespoke Terrain}

If you have any unique terrain features in your collection, discuss
with your opponent before the battle begins what parts of the terrain
feature are obstacles, platforms, unscalable, etc.